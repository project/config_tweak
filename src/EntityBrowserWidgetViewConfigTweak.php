<?php

namespace Drupal\config_tweak;

use Drupal\entity_browser\Plugin\EntityBrowser\Widget\View;
use Drupal\entity_browser\WidgetValidationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Uses a view to provide entity listing in a browser's widget.
 *
 * If is needed to use optional dependencies then in appropriate
 * entity_browser.browser.*.yml files in section 'widgets' -> some
 * widget name -> 'settings:', can be used the option:
 * 'dependencies_optional: yes'. Like:
 * (example for entity_browser.browser.boxfooterbrowser.yml)
 * ...
 * widgets:
 *   ...
 *   widget-box-footer-browser-box-menu:
 *   settings:
 *     dependencies_optional: yes
 *     view: boxsimple_selection
 *     ...
 */
class EntityBrowserWidgetViewConfigTweak extends View {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_browser.widget_validation'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Constructs a new View object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, AccountInterface $current_user, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager, $current_user);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];
    if ($this->configuration['view']) {
      if (!$this->configFactory->get('config_tweak.settings')->get('break_entity_browser_dependencies') || (isset($this->configuration['dependencies_optional']) && $this->configuration['dependencies_optional'] == 'yes')) {
        $view = $this->viewStorage->load($this->configuration['view']);
        if ($view) {
          $dependencies[$view->getConfigDependencyKey()] = [$view->getConfigDependencyName()];
        }
      }
    }
    return $dependencies;
  }

}
