<?php

namespace Drupal\config_tweak;

use Drupal\Core\Field\FieldTypePluginManager;

/**
 * Extend FieldTypePluginManager class.
 *
 * Replace class definition for entity_reference type fields,
 * for reimplementing calculateDependencies() method.
 *
 * Replacing the class EntityReferenceItem by using the hook
 * hook_entity_bundle_field_info_alter() doesn't work as expected,
 * so is used the plugin.manager.field.field_type service for replacing
 * the required class.
 *
 * @ingroup field_types
 */
class FieldTypePluginManagerConfigTweak extends FieldTypePluginManager {

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    // Fetch definitions if they're not loaded yet.
    if (!isset($this->definitions)) {
      $this->getDefinitions();
    }

    $definition = $this->doGetDefinition($this->definitions, $plugin_id, $exception_on_invalid);

    if ($definition['id'] == 'entity_reference') {
      // Use our own EntityReferenceItem::calculateDependencies() implementation
      // for entity_reference type fields.
      $definition['class'] = '\Drupal\config_tweak\EntityReferenceItemConfigTweak';
    }

    return $definition;
  }

}
