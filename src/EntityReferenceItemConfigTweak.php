<?php

namespace Drupal\config_tweak;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Eliminate optional dependencies requirement.
 *
 * If is needed to use optional dependencies then in appropriate
 * field.field.*.yml files in section 'settings:' -> 'handler_settings:',
 * can be used the option: 'dependencies_optional: yes'. Like:
 * (example for field.field.paragraph.box_footer.field_box.yml)
 * ...
 * settings:
 *   handler_settings:
 *     dependencies_optional: yes
 *     target_bundles:
 *      ...
 */
class EntityReferenceItemConfigTweak extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function calculateDependencies(FieldDefinitionInterface $field_definition) {
    $dependencies = [];
    $entity_type_manager = \Drupal::entityTypeManager();
    $target_entity_type = $entity_type_manager->getDefinition($field_definition->getFieldStorageDefinition()->getSetting('target_type'));

    // Depend on default values entity types configurations.
    if ($default_value = $field_definition->getDefaultValueLiteral()) {
      $entity_repository = \Drupal::service('entity.repository');
      foreach ($default_value as $value) {
        if (is_array($value) && isset($value['target_uuid'])) {
          $entity = $entity_repository->loadEntityByUuid($target_entity_type->id(), $value['target_uuid']);
          // If the entity does not exist do not create the dependency.
          // @see \Drupal\Core\Field\EntityReferenceFieldItemList::processDefaultValue()
          if ($entity) {
            $dependencies[$target_entity_type->getConfigDependencyKey()][] = $entity->getConfigDependencyName();
          }
        }
      }
    }

    $handler = $field_definition->getSetting('handler_settings');
    if (!\Drupal::config('config_tweak.settings')->get('break_entity_reference_dependencies') || (isset($handler['dependencies_optional']) && $handler['dependencies_optional'] == 'yes')) {

      // Depend on target bundle configurations. Dependencies for
      // 'target_bundles' also covers the 'auto_create_bundle' setting, if any,
      // because its value is included in the 'target_bundles' list.
      if (!empty($handler['target_bundles'])) {
        if ($bundle_entity_type_id = $target_entity_type->getBundleEntityType()) {
          if ($storage = $entity_type_manager->getStorage($bundle_entity_type_id)) {
            foreach ($storage->loadMultiple($handler['target_bundles']) as $bundle) {
              $dependencies[$bundle->getConfigDependencyKey()][] = $bundle->getConfigDependencyName();
            }
          }
        }
      }
    }

    return $dependencies;
  }

}
