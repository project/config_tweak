<?php

namespace Drupal\config_tweak\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings for config tweak.
 *
 * @internal
 */
class ConfigTweakSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_tweak.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_tweak.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config_tweak.settings');
    $form['circle_dependency'] = [
      '#type' => 'details',
      '#title' => $this->t('Break circular dependency'),
      '#open' => TRUE,
    ];
    $form['circle_dependency']['break_entity_reference_dependencies'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('In entity reference'),
      '#default_value' => $config->get('break_entity_reference_dependencies'),
      '#description' => $this->t('Remove dependencies on entity reference target.'),
    ];
    if ($this->moduleHandler->moduleExists('entity_browser')) {
      $form['circle_dependency']['break_entity_browser_dependencies'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('In entity browser'),
        '#default_value' => $config->get('break_entity_browser_dependencies'),
        '#description' => $this->t('Remove dependencies on views widgets.'),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('config_tweak.settings')
      ->set('break_entity_reference_dependencies', $form_state->getValue('break_entity_reference_dependencies'))
      ->set('break_entity_browser_dependencies', $form_state->getValue('break_entity_browser_dependencies'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
