Shortly, the module helps to make “Configuration updates report” — “clean again”:).

Currently, the module implements next tweaks:

1. **Break circular dependency in entity reference** (Remove dependencies on entity reference target.)

    If it is still needed to use optional dependencies, in some particular case, then in appropriate
    field.field.*.yml files in section 'settings:' -> 'handler_settings:',
    can be used the option: 'dependencies_optional: yes'. Like:
    (example for field.field.paragraph.box_footer.field_box.yml)

    ```
     settings:
       handler_settings:
         dependencies_optional: yes
         target_bundles:
    ```

2. **Break circular dependency in Entity Browser** (Remove dependencies on views widget.)

    NOTE: For using the module along with Entity Browser, should be applied the patch
    from the issue https://www.drupal.org/project/entity_browser/issues/3035036 .

    If it is still needed to use optional dependencies, in some particular case, then in appropriate
    entity_browser.browser.*.yml files in section 'widgets' -> some
    widget name -> 'settings:', can be used the option:
    'dependencies_optional: yes'. Like:
    (example for entity_browser.browser.boxfooterbrowser.yml)

    ```
    widgets:
       ...
       widget-box-footer-browser-box-menu:
       settings:
         dependencies_optional: yes
         view: boxsimple_selection
    ```

Installation and configuration.

1. Enable the module like any other.
2. Go to Configuration -> Development -> Config tweak ( /admin/config/development/config_tweak ) and change
   the options as needed.
